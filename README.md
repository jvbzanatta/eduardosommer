### Checklist final de entrega
1. Já avaliei o escopo (tirei as dúvidas com cliente ou equipe técnica, ou não tive dúvidas); 
2. Fiz uma estimativa em HORAS do desenvolvimento de TODO o projeto ; 
3. Também fiz uma estimativa em DIAS do prazo de entrega; 
4. Já tenho o link da minha tabela com as horas que utilizei apontadas; 
5. Realizei pelo menos 3 das atividades desenvolvidas e funcionais, com código no repositório gitlab fornecido; 
6. Organizei todas as informações acima no Read.me do repositório; 
7. Gravei um vídeo simples.



### Tecnologias Utilizadas

Foi utilizado o Visual Studio 2017, foi criado uma API em c# para o backend, utilizando o EntityFramework, e também foi utilizado o SWAGGER para mapeamento.
No front-end foi utilizado Asp.Net MVC juntamente com Javascript e utilizando Material Theme para o auxílio do layout.
Foi utilizado o banco de dados SQL.


### O projeto esta estruturado da seguinte forma

* ToDo: É o webapp do projeto.
* ToDo.Api: Onde está configurado a Api que o WebApp "chama".
* ToDo.Domain: Onde está as entidades e suas respectivas lógicas para persistências.
* ToDo.Infra: Onde está o Context com as informações da estrutra do banco e sua migrations.
* ToDo.IoC: Responsável pela configuração da injeção de dependencia.


### Executando o projeto

- Para executar o projeto é necessário ter o VS2017 ou VS2019 instalado juntamente com .Framework 4.6.1.
- SQL server
- Configurar o Web.config do projeto ToDo.Api
    - Na linha <add name="TODOConnectionString" connectionString="Server=DESKTOP-K70LVOK;Database=TODO......
        alterar o Server=DESKTOP-K70LVOK para seu respectivo servidor do SQL
- Para executar localmente deverá rodar os dois projetos: ToDo(webapp) e o ToDo.Api
- OBS: Verificar o arquivo Todo/Utils/ServiceApiUtil/ e confirmar se a porta da aplicação onde está definido na variável ApiBaseUrl continua a mesma
- PRONTO sua aplicação está pronta para ser executada 


#### Acompanhamento das horas https://docs.google.com/spreadsheets/d/1s9SCL4xLfhtIIGc75Z5t_awuL0ULtLSz3zPxiVgQcRA/edit?usp=sharing
## Demonstração do sistema https://youtu.be/eC1673PgQ-8
### Meu e-mail para contato é dusommer@hotmail.com ou (47)99737-3398(whats resposta quase imediata)






